var gulp          = require('gulp');
var plumber       = require('gulp-plumber');
var wiredep       = require('wiredep').stream;
var sourcemaps    = require('gulp-sourcemaps');
var sass          = require('gulp-sass');
var postcss       = require('gulp-postcss');
var autoprefixer  = require('autoprefixer');
var mqpacker      = require('css-mqpacker');
var config        = require('../config').styles;

function onError (err) {
  console.log(err);
  this.emit('end');
}

var processors = [
  autoprefixer(config.postcss.autoprefixer),
  mqpacker(config.postcss.mqpacker)
];

gulp.task('styles', function () {
  return gulp.src(config.src)
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(wiredep(config.wiredep))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(config.dest));
});
