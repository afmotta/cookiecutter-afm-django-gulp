var gulp        = require('gulp');
var bowerFiles  = require('main-bower-files');
var gulpFilter  = require('gulp-filter');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var cssnano     = require('gulp-cssnano');
var config      = require('../config').optimize;


gulp.task('optimize', function () {
  var files = bowerFiles();
  files.push(config.js.src);
  files.push(config.css.src);
  var mainFilter = gulpFilter(['**/*.js', '**/*.css']);
  var jsFilter = gulpFilter('**/*.js', {restore: true});
	var cssFilter = gulpFilter('**/*.css', {restore: true});

  return gulp.src(files)
    .pipe(mainFilter)
		.pipe(jsFilter)
		.pipe(concat(config.js.fileName, config.js.concat))
    .pipe(uglify(config.js.uglify))
		.pipe(jsFilter.restore)
		.pipe(cssFilter)
    .pipe(concat(config.css.fileName, config.css.concat))
    .pipe(cssnano(config.css.cssnano))
		.pipe(cssFilter.restore)
		.pipe(gulp.dest(config.dest));
});
