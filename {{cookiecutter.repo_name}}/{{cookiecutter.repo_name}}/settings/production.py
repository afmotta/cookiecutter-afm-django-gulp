from .base import *


DEBUG = False
TEMPLATES['DEBUG'] = DEBUG

ALLOWED_HOSTS = ['*']

ADMINS = (
    ('{{cookiecutter.author_name}}', '{{cookiecutter.email}}'),
)

MANAGERS = ADMINS
